;utility
(defun midi-to-pitch 
    (n)
    (make-pitch 
        (midi-to-freq n)))

(defun every-nth 
    (ls n)
    (loop for i from 0 to 
        (- 
            (list-length ls) 1) if
        (=  0 
            (mod i n)) collect 
        (nth i ls)))

(defun distance-to-power-of-2 
    (x)
    (abs 
        (- x 
            (nearest-power-of-2 x t))))

(defun loop-for-n-steps 
    (seq n)
    (let* 
        (
            (circle 
                (make-cscl seq)))
        (loop for i from 1 to n for e = 
            (get-next circle) collect e)))

; make sure that a sum of list equals to a certain value by adding or subtracting
; from the last value in the list
(defun fill-up-last 
    (ls sum)
    (let* 
        (
            (diff 
                (- sum 
                    (reduce '+ ls))))
        (setf 
            (first 
                (last ls)) 
            (+ diff 
                (first 
                    (last ls)))) ls))

; scale every value in a list so that they sum up to a new value
(defun scale-proportions 
    (proportions new)
    (let* 
        (
            (sum 
                (reduce '+ proportions)))
        (if 
            (< 
                (length proportions) 1)
'
            ()
            (if 
                (< 0 sum)
                (fill-up-last 
                    (loop for p in proportions collect 
                        (rescale p 0 sum 0 new)) new)
                (fill-up-last 
                    (loop for p in proportions collect 0) new)))))

;print
(defun pitches-to-ids 
    (pitches)
    (mapcar 
        (lambda 
            (x) 
            (id x)) pitches))

(defun print-pitches 
    (pitches)
    (progn
        (print 
            (pitches-to-ids pitches)) pitches))
