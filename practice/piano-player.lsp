(defclass piano-player 
  (string-instrument-player)
  (
    (durations-seeder :initform 
      (make-cscl 
        (loop for i from 1 to 8 collect i)) :accessor durations-seeder)
    (lfl-durations :initform 
      (make-l-for-lookup 'l-sys-durations '
        (
          (1 
            (
              (14)))
          (2 
            (
              (2)))
          (3 
            (
              (13)))
          (4 
            (
              (3)))
          (5 
            (
              (12)))
          (6 
            (
              (4)))
          (7 
            (
              (11)))
          (8 
            (
              (5)))) '
        (
          (1 
            (1 2))
          (2 
            (3 2))
          (3 
            (3 4))
          (4 
            (5 4))
          (5 
            (5 6))
          (6 
            (7 6))
          (7 
            (7 8))
          (8 
            (1 8)))) :accessor lfl-durations)
    (amps-seeder :initform 
      (make-cscl 
        (loop for i from 1 to 4 collect i)) :accessor amps-seeder)
    (lfl-amps :initform 
      (make-l-for-lookup 'l-sys-amps '
        (
          (1 
            (
              (25) 
              (30)))
          (2 
            (
              (50) 
              (65)))
          (3 
            (
              (75) 
              (90)))
          (4 
            (
              (110) 
              (127)))) '
        (
          (1 
            (1 2 3 4))
          (2 
            (3 2 1))
          (3 
            (1 1 1 1 4))
          (4 
            (1)))) :accessor lfl-amps)))

(defun make-piano-player 
  (scale melody)
  (make-instance 'piano-player :lowest-note 26 :highest-note 108 :note-scale scale :degrees melody))

(defmethod get-durations 
  (
    (obj piano-player) n)
  (let* 
    (
      (durations 
        (do-lookup 
          (lfl-durations obj) 
          (get-next 
            (durations-seeder obj)) 
          (* n 2)))) durations))

(defmethod get-amps 
  (
    (obj piano-player) n)
  (let* 
    (
      (amps 
        (do-lookup 
          (lfl-amps obj) 
          (get-next 
            (amps-seeder obj)) 
          (* n 2)))) amps))

(defmethod get-leading 
  (
    (obj piano-player) degree)
  (let* 
    (
      (leading 
        (- 
          (- 
            (length 
              (crop-scale obj)) 1) degree))) leading))

(defmethod get-leadings 
  (
    (obj piano-player) n)
  (let* 
    (
      (leadings 
        (loop for d in 
          (degrees obj) collect 
          (get-leading obj d))))
    (loop-for-n-steps leadings n)))

(defmethod get-pitches2 
  (
    (obj piano-player) n sizes)
  (let* 
    (
      (chords 
        (apply-scale 
          (get-chords obj n sizes 2) 
          (crop-scale obj)))
      (leadings 
        (apply-scale 
          (get-leadings obj n) 
          (crop-scale obj))))
    (loop for c in chords for l in leadings append 
      (list c l))))

(defmethod get-midi-events 
  (
    (obj piano-player) n sizes)
  (let* 
    (
      (pitches 
        (get-pitches2 obj n sizes))
      (durations 
        (get-durations obj n))
      (amps 
        (get-amps obj n)))
    (assert 
      (= 
        (length pitches) 
        (length durations)))
    (loop for p in pitches for d in durations for a in amps append 
      (make-midi-event p a d 0 127))))
